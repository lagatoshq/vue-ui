# vue-ui

[![FOSSA Status](https://app.fossa.io/api/projects/custom%2B7075%2Fgithub.com%2Felectricfeelco%2Fvue-ui.svg?type=shield)](https://app.fossa.io/projects/custom%2B7075%2Fgithub.com%2Felectricfeelco%2Fvue-ui?ref=badge_shield)

> UI library for vue, forked from [vue-starter template](https://github.com/devCrossNet/vue-starter)

![](./images/example1.png)

## Installation
```
npm install vue-ui
```
vue-ui can be used as a module in both CommonJS and ES modular environments.

### After that, you can use it in your Vue components:

```html
```

## Changelog

See the GitHub [release history](https://github.com/gwenaelp/vue-ui/releases).

## Contributing

See [CONTRIBUTING.md](.github/CONTRIBUTING.md).

[![](https://codescene.io/projects/3808/status.svg) Get more details at **codescene.io**.](https://codescene.io/projects/3808/jobs/latest-successful/results)

