import Vue from 'vue';

const eventBus = new Vue();

window['EventBus'] = eventBus;

export const EventBus: Vue = eventBus;
