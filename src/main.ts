import VeeValidate from 'vee-validate';
import { HttpService } from './services/HttpService';

import VueAccordion from './components/VueAccordion/VueAccordion.vue';
import VueAccordionItem from './components/VueAccordion/VueAccordionItem/VueAccordionItem.vue';
import VueAutocomplete from './components/VueAutocomplete/VueAutocomplete.vue';
import VueBadge from './components/VueBadge/VueBadge.vue';
import VueBreadcrumb from './components/VueBreadcrumb/VueBreadcrumb.vue';
import VueButton from './components/VueButton/VueButton.vue';
import VueCalendar from './components/VueCalendar/VueCalendar.vue';
import VueCarousel from './components/VueCarousel/VueCarousel.vue';
import VueCheckbox from './components/VueCheckbox/VueCheckbox.vue';
import VueCollapse from './components/VueCollapse/VueCollapse.vue';
import VueDataTable from './components/VueDataTable/VueDataTable.vue';
import VueDatePicker from './components/VueDatePicker/VueDatePicker.vue';
import VueDateRangePicker from './components/VueDateRangePicker/VueDateRangePicker.vue';
import VueGrid from './components/VueGrid/VueGrid.vue';
import VueGridItem from './components/VueGridItem/VueGridItem.vue';
import VueGridRow from './components/VueGridRow/VueGridRow.vue';
import VueInput from './components/VueInput/VueInput.vue';
import VueLoader from './components/VueLoader/VueLoader.vue';
import VueMarkdown from './components/VueMarkdown/VueMarkdown.vue';
import VueModal from './components/VueModal/VueModal.vue';
import VueNavBar from './components/VueNavBar/VueNavBar.vue';
import VueNewsletter from './components/VueNewsletter/VueNewsletter.vue';
import VueNotificationStack from './components/VueNotificationStack/VueNotificationStack.vue';
import VuePagination from './components/VuePagination/VuePagination.vue';
import VueCard from './components/VueCard/VueCard.vue';
import VueCardBody from './components/VueCard/VueCardBody/VueCardBody.vue';
import VueCardFooter from './components/VueCard/VueCardFooter/VueCardFooter.vue';
import VueCardHeader from './components/VueCard/VueCardHeader/VueCardHeader.vue';
import VueSelect from './components/VueSelect/VueSelect.vue';
import VueSlider from './components/VueSlider/VueSlider.vue';
import VueTabGroup from './components/VueTabGroup/VueTabGroup.vue';
import VueTabItem from './components/VueTabGroup/VueTabItem/VueTabItem.vue';
import VueToggle from './components/VueToggle/VueToggle.vue';
import VueTooltip from './components/VueTooltip/VueTooltip.vue';
import VueTruncate from './components/VueTruncate/VueTruncate.vue';
import { AutocompleteOptionsFixture } from './components/VueAutocomplete/fixtures/IAutocompleteFixture';
import { IAutocompleteOption } from './components/VueAutocomplete/IAutocompleteOption';
import {
  addNotification,
  INotification
} from './components/VueNotificationStack/utils';
import {
  dataTableDataFixture,
  dataTableHeaderFixture
} from './components/VueDataTable/DataTableFixtures';
import VueSwing from 'vue-swing';
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import VueTagsInput from '@johmun/vue-tags-input';

const components = {
  Loading,
  VueAccordion,
  VueAccordionItem,
  VueAutocomplete,
  VueBadge,
  VueButton,
  VueBreadcrumb,
  VueCalendar,
  VueCarousel,
  VueCheckbox,
  VueCollapse,
  VueDataTable,
  VueDatePicker,
  VueDateRangePicker,
  VueGrid,
  VueGridItem,
  VueGridRow,
  VueInput,
  VueLoader,
  VueNewsletter,
  VueMarkdown,
  VueModal,
  VueNavBar,
  VueNotificationStack,
  VuePagination,
  VueCard,
  VueCardBody,
  VueCardFooter,
  VueCardHeader,
  VueSelect,
  VueSlider,
  VueSwing,
  VueTabGroup,
  VueTabItem,
  VueTagsInput,
  VueToggle,
  VueTooltip,
  VueTruncate
};

const LibraryModule = {
  ...components,

  install(Vue, options) {
    if (!options.store) {
      console.error(
        "You must provide the vuex store as an option of Vue.use('VueUi')"
      );
    }
    HttpService.store = options.store;
    Vue.use(Loading);
    Vue.use(VeeValidate, { inject: false });
    // Register components with vue
    Object.keys(components).forEach(name => {
      Vue.component(name, components[name]);
    });
  }
};

// Export library
export default LibraryModule;

// Export components
// export components;
